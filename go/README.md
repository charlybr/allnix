# Langage Go

## Introduction

* [A tour of Go](https://tour.golang.org/welcome/1)
* [Go by example](https://gobyexample.com/)
* [Getting started with Go guide](https://dominicstpierre.com/getting-started-with-go-guide)
* [An intro to Go for non-Go developers](https://benhoyt.com/writings/go-intro/)
* [12 Weeks of learning Go](https://www.martincartledge.io/history-of-go-variables-and-types/)

## Apprentissage

* [A few ideas for your next Go project](https://smoqadam.me/posts/a-few-ideas-for-your-next-go-project/)  

## Organisation du code

* [Is there any conventionally accepted repo that is representative of well designed go code ?](https://www.reddit.com/r/golang/comments/uqwggr/is_there_any_conventionally_accepted_repo_that_is/)

## Frameworks

* [Web framework](https://github.com/mingrammer/go-web-framework-stars)
