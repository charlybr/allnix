# Networking

## Apprendre le réseau

* [Le Réseau de zéro (partie 2)](https://lafor.ge/wireguard-1/)
* [Introduction to Linux interfaces for virtual networking](https://developers.redhat.com/blog/2018/10/22/introduction-to-linux-interfaces-for-virtual-networking) *October 22, 2018*

## Protocoles

Illustrated connections, protocols byte-by-byte, by Michael Driscoll :
* QUIC: https://quic.xargs.org
* DTLS: https://dtls.xargs.org
* TLS 1.2: https://tls12.xargs.org
* TLS 1.3: https://tls13.xargs.org
