# Docker


## Articles

* [Docker en production : le cas d'usage de Bearstech](https://bearstech.com/societe/blog/docker-en-production-le-cas-dusage-de-bearstech/)
* [Why is Exposing the Docker Socket a Really Bad Idea?](https://blog.quarkslab.com/why-is-exposing-the-docker-socket-a-really-bad-idea.html)
* [The worst so-called “best practice” for Docker](https://pythonspeed.com/articles/security-updates-in-docker/)
* [81 Docker Command Cheat Sheet with Description](https://www.fosstechnix.com/docker-command-cheat-sheet/)
* [Running Docker images without Docker](https://jakub-m.github.io/2022/09/10/docker.html)
* [How to Fix and Debug Docker Containers Like a Superhero](https://www.docker.com/blog/how-to-fix-and-debug-docker-containers-like-a-superhero/)
* [Explain it to me like I am a 5-year-old: What are Docker, Image, and Containers](https://avs431.medium.com/explain-it-to-me-like-i-am-a-5-year-old-what-are-docker-image-and-containers-b18db4863cb1) *2020-04-10*

## Bonnes pratiques

* [Top 20 Dockerfile Best Practices](https://dzone.com/articles/top-20-dockerfile-best-practices)
* [How we reduced our docker build times by 40%](https://medium.com/datamindedbe/how-we-reduced-our-docker-build-times-by-40-afea7b7f5fe7)

## Tools

* [LazyDocker](https://github.com/jesseduffield/lazydocker)

## Snippets

### Images

* lister les images

```
$ docker image ls 
REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
hello-world   latest    feb5d9fea6a5   9 months ago   13.3kB
```

```
$ docker image rm feb5d9fea6a5
Untagged: hello-world:latest
Untagged: hello-world@sha256:13e367d31ae85359f42d637adf6da428f76d75dc9afeb3c21faea0d976f5c651
Deleted: sha256:feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412
Deleted: sha256:e07ee1baac5fae6a26f30cabfe54a36d3402f96afda318fe0a96cec4ca393359
```


### Container

* lister les containers actifs

```
$ docker container ls
REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
hello-world   latest    feb5d9fea6a5   9 months ago   13.3kB
```

* lister tous les containers

```
$ docker container ls -a
CONTAINER ID   IMAGE         COMMAND    CREATED         STATUS                     PORTS     NAMES
af334d50f82f   hello-world   "/hello"   2 minutes ago   Exited (0) 2 minutes ago             distracted_murdock
21a29b7540d6   hello-world   "/hello"   3 minutes ago   Exited (0) 3 minutes ago             relaxed_golick
```

* lister les containers qui ont terminé

```
$ docker ps --all -q -f status=exited
af334d50f82f
21a29b7540d6
```

et les effacer
```
$ docker rm $(docker ps --all -q -f status=exited)
```

## Docker compose

en référence au `doccker-compose.yml` courant :

* crée et démarre les containers
```
$ docker compose up 
```

* démarrer un container spécifique
```
$ docker compose run node "yarn" "encore" "dev" "--watch"
```

* construire un service
```
$ docker compose build node
```

* affiche l'output du build
```
$ docker compose build node --progress=plain
```

* ne pas utiliser le cache
```
$ docker compose build node --progress=plain --no-cache
```

### Misc

* Afficher l'espace disque utilisé par docker
```
$ docker system df --format 'table {{.Type}}\t{{.TotalCount}}\t{{.Size}}'
TYPE            TOTAL     SIZE
Images          12        4.615GB
Containers      4         2B
Local Volumes   3         202MB
Build Cache     59        3.436kB
```

