# Postgresql

## Tools

* [pgmetrics](https://pgmetrics.io/) Easily collect and report PostgreSQL metrics for scripting, automation and troubleshooting.
  * [pgDash](https://pgdash.io/) frontend saas de pgmetrics
* [pg_activity](https://github.com/dalibo/pg_activity) pg_activity is a top like application for PostgreSQL server activity monitoring.
* [Mathesar](https://github.com/centerofci/mathesar) Web application providing an intuitive user experience to databases.

## Articles

* [Create and Seed Your Postgres Database in Minutes with Docker](https://dev.to/codewithluke/create-and-seed-your-postgres-database-in-minutes-with-docker-1i11)
* [Extreme PostgreSQL](https://thebuild.com//presentations/xtreme-sfpug-2023-02-08.pdf)


## Psql 

* se connecter à une db

```
psql> \c postgres 
```

* lister les extensions installées

```
dbname=> \dx
                 List of installed extensions
  Name   | Version |   Schema   |         Description
---------+---------+------------+------------------------------
 plpgsql | 1.0     | pg_catalog | PL/pgSQL procedural language
(1 row)
```

* lister les bases

```
postgres=# \l
                                   List of databases
    Name    |  Owner   | Encoding |   Collate   |    Ctype    |    Access privileges
------------+----------+----------+-------------+-------------+-------------------------
 postgres   | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 template0  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres            +
            |          |          |             |             | postgres=CTc/postgres
 template1  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres            +
            |          |          |             |             | postgres=CTc/postgres
```

* lister les tables
```
icinga2=# \dt
                         List of relations
 Schema |                  Name                  | Type  |  Owner
--------+----------------------------------------+-------+---------
 public | icinga_acknowledgements                | table | icinga2
 public | icinga_commands                        | table | icinga2
 public | icinga_commenthistory                  | table | icinga2
 public | icinga_comments                        | table | icinga2
...
```


