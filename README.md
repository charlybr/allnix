# allnix

all things *nix

* Awesome Free / Open Source Alternatives (to common SaaS products) for Business Use https://github.com/sfermigier/awesome-foss-alternatives
* [Automation](automation/README.md)
* [Coding](coding/README.md)
* [Bases de données](db/README.md)
* [Diagnostique](pages/diag/README.md)
* [Distro Linux](pages/distros/README.md)
* [Infrastructure as Code](iac/README.md)
* [Ligne de commande](pages/cmdline/README.md)
* [Monitoring](monitoring/README.md)
* [Web](pages/web/README.md)

## Outils

* [The POSIX Shell And Utilities](https://shellhaters.org/)

## Histoire et articles *nix 

* [Unix Philosophy: A Quick Look at the Ideas that Made Unix](https://klarasystems.com/articles/unix-philosophy-a-quick-look-at-the-ideas-that-made-unix/)
* [Forgetting the history of Unix is coding us into a corner](https://www.theregister.com/2024/02/16/what_is_unix/)
* [The Berkeley Software Distribution](https://www.abortretry.fail/p/the-berkley-software-distribution)
* [The early days of Linux](https://lwn.net/Articles/928581/)
* [Open Sources: Voices from the Open Source Revolution](https://www.oreilly.com/openbook/opensources/book/kirkmck.html)