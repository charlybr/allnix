# Proxmox

## Configuration 

* Installation de l'agent
```
$ apt-get install qemu-guest-agent
$ systemctl start qemu-guest-agent
```
L'agent permet par exemple d'avoir l'adresse IP du guest dans l'interface web.

## Réseau

* [Network Configuration](https://pve.proxmox.com/wiki/Network_Configuration)

## Articles

* [Retour d’expérience avec Proxmox, Docker et Portainer](https://www.cachem.fr/proxmox-docker-portainer/) *2023-06-28*
