# Vim

start with running `vimtutor`

* [Vim help files](https://vimhelp.org/)
* [Raccourcis clavier](vim-shortcuts.md)

## Tuto

* [Vim galore, everything you need to know about vim](https://github.com/mhinz/vim-galore)

## Plugins

* A minimalist Vim plugin manager : https://github.com/junegunn/vim-plug

## Themes

### Ajouter un thème

Source du thème https://github.com/AlessandroYorba/Sierra

```
$ mkdir .vim/colors
$ cd .vim/colors
$ curl -O https://raw.githubusercontent.com/AlessandroYorba/Sierra/master/colors/sierra.vim
```
dans `.vimrc`:
```
colorscheme sierra
```
