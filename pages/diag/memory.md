# Diagnostique - mémoire

## Mémoire utilisée par les processus

### avec top

Par défaut top trie par consommation CPU.
Pour changer les filtres, il faut faire <kbd>shift</kbd> <kbd>f</kbd>  pour entrer en mode configuration.
  * déplacez-vous avec les flèches pour vous placer devant le nom d'un champ.
  * la touche <kbd>s</kbd> permet de sélectionner le champ utilisé pour le tri. Malheureusement il n'y a pas d'artefact visuel lorsqu'on presse la touche <kbd>s</kbd>
  * <kbd>esc</kbd> permet de retourner à l'interface de visualisation

Il existe des raccourcis de base pour changer le mode de tri directement dans l'interface de visualisation :
* <kbd>P</kbd> : tri par utilisation CPU
* <kbd>M</kbd> : tri par utilisation mémoire
* <kbd>T</kbd> : tri par temps d'execution

### avec ps

```
$ ps aux | sort -rnk 4 | head -5
mysql        910  1.1  3.1 2903988 258808 ?      Ssl  May31 857:47 /usr/sbin/mysqld
www-data  751038  0.0  1.6 357164 132496 ?       S    Jul11   7:58 php-fpm: pool www
www-data  751037  0.0  1.6 358908 135164 ?       S    Jul11   8:01 php-fpm: pool www
www-data  554080  0.0  1.5 358440 123892 ?       S    Jul09   9:00 php-fpm: pool www
root     1923243  0.0  0.8 120400 65736 ?        S<s  Jul21   0:04 /lib/systemd/systemd-journald
```

Ici on utilise `sort` directement sur la sortie de `ps` avec les options suivantes :
* `-r` : ordre inverse)
* `-n` : tri numérique)
* `-k 4` : tri sur le 4e champ

### Avec ps et un affichage en Gb

```
$ ps ax -o vsz,comm | sort -rnk 1 | head -10 | awk '{$1=int(100 * $1/1024/1024)/100"GB";}{ print ;}'
2.52GB mysqld
0.96GB salt-minion
0.85GB minio
0.75GB icinga2
0.34GB php-fpm7.4
0.27GB php-fpm7.4
0.27GB php-fpm7.4
0.26GB php-fpm7.4
0.26GB php-fpm7.4
0.26GB php-fpm7.4
```

Le switch `-o` permet de sélectionner les champs que l'on veut afficher, ici `vsz` pour la mémoire virtuelle consommée par le processus et `comm` pour le processus.


### Outils

* https://unix.stackexchange.com/questions/554/how-to-monitor-cpu-memory-usage-of-a-single-process
* https://github.com/astrofrog/psrecord
