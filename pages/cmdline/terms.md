# Terminaux

[← Ligne de commande](README.md)

---


## GUI

* [Terminator](https://terminator-gtk3.readthedocs.io/en/latest/)
* [Tilix](https://github.com/gnunn1/tilix/) *Tilix is an advanced GTK3 tiling terminal emulator*
* [Terms retro](https://github.com/Swordfish90/cool-retro-term)

## Terminaux distants

* [tmux](tmux.md) *tmux est un multiplexeur de terminaux*
* screen

## Web

* [ttyd](https://github.com/tsl0922/ttyd) *ttyd is a simple command-line tool for sharing terminal over the web.*
* [websocketd](http://websocketd.com/) It takes care of handling the WebSocket connections, launching your programs to handle the WebSockets, and passing messages between programs and web-browser.
* [xterm.js](https://github.com/xtermjs/xterm.js/) A terminal for the web

