# AWK

## Délimitateur de découpage

On peut définir le séparateur de champ de deux façons :

* `-F`

```
$ echo 192.168.0.1 | awk -F\. '{print $4}'
1
```

* la variable `FS` (field separator)

```
$ echo 192.168.0.1 | awk 'BEGIN { FS = "." } ; {print $4}'
1
```


On peut utiliser plusieurs caractères comme séparateur :

```
$ echo 192.168,0.1 | awk -F'[,.]' '{print $1" "$2" "$3" "$4}'
```

## Afficher un bar chart par ligne relatif à la taille des données transférées pour une requête dans un access log

```
awk -v cols=$(tput cols) '{e=int(log($10)*5); print("\x1b[42m" substr($0, 1, e) "\x1b[0m" substr($0, e+1) )}' access.log | less -SR
```
