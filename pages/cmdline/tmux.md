# Tmux

[← Terminaux](terms.md)

---


Il faut utiliser `ctrl+b` pour activer le mode commande.

* https://leimao.github.io/blog/Tmux-Tutorial/
* https://medium.com/hackernoon/a-gentle-introduction-to-tmux-8d784c404340
* [tmux cheatsheet](https://gist.github.com/henrik/1967800)
* [tmux 2](https://medium.com/pragmatic-programmers/table-of-contents-8d309bb8a661) by The Pragmatic Programmers
* [tmux-power - Tmux Powerline Theme](https://github.com/wfxr/tmux-power)

## Sessions

* start a session

```
tmux new
```

* leave current session

```
ctrl-b d
[detached (from session 0)]
```

* name new session

```
tmux new -s [name of session]
```

* show current sessions


```
tmux ls
0: 1 windows (created Mon Jun 21 09:48:39 2021)
monito: 1 windows (created Mon Jun 21 09:52:11 2021)
```

works also with `tmux list-sessions`


* attach session by name

```
tmux a -t monito
```

attach by number
```
tmux a -t 0
```

* rename a session 
```
tmux rename-session -t old-session-name new-session-name
tmux rename-session -t 0 new-session-name
```

## Windows

* split pane horizontally

```
ctrl+b "
```

* split vertically

```
ctrl+b %
```

* move between panes

```
ctrl+b [arrow key]
```

From there we can type resize-pane followed by a direction flag: `-U` for up, `-D` for down `-L` for left and `-R` for right. The last part is the number of lines to move it over by.
As an example, if we are in the top pane and want to expand it down by 2 lines, we would do the following:

```
ctrl+b :
resize-pane -D 2
```

* create new window

```
ctrl+b c
```

* move between windows

`ctrl+b n` next window
`ctrl+b p` previous window
`ctrl+b [number]` window by its number
`ctrl+b w` list windows

* rename window

`ctrl-b ,`


* scroll

```
ctrl-b [
```

`q` pour quitter le mode scroll

activer le scroll souris :

```
set -g mode-mouse on 
```

## Config 

```
:source-file ~/.tmux.conf
```


