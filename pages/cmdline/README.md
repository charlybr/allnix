# Utilisation de la ligne de commande

## Shells

* [Bash](bash.md)
* [Zsh](zsh.md)

### Prompts

* [Starship](https://starship.rs/) *The minimal, blazing-fast, and infinitely customizable prompt for any shell!*

## Outils courants

* [awk](awk.md)
* [sort](sort.md)
* [Command not found](https://command-not-found.com/) *Install any command on any operating system.*


### Nouveaux Outils

* [A list of new(ish) command line tools](https://jvns.ca/blog/2022/04/12/a-list-of-new-ish--command-line-tools/)

## Terms

* [Terminaux](terms.md)

## Misc

* [What goes in ~/.profile and ~/.bashrc?](https://askubuntu.com/questions/1411833/what-goes-in-profile-and-bashrc)
