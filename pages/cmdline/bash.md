# Bash

## Sources de liens

* https://github.com/awesome-lists/awesome-bash

## Articles pour apprendre

* [Bash Scripting: Everything you need to know about Bash-shell programming](https://medium.com/sysf/bash-scripting-everything-you-need-to-know-about-bash-shell-programming-cd08595f2fba)
* https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
* [Linux bash exit status and how to set exit status in bash](https://www.cyberciti.biz/faq/linux-bash-exit-status-set-exit-statusin-bash/)
* [Bash best practices](https://bertvv.github.io/cheat-sheets/Bash.html)
* https://github.com/bobbyiliev/introduction-to-bash-scripting
