# Zsh

Zsh is a shell designed for interactive use, although it is also a powerful scripting language.

* https://www.zsh.org/

## Configuration

* [Oh My Zsh + PowerLevel10k = 😎 terminal](https://dev.to/abdfnx/oh-my-zsh-powerlevel10k-cool-terminal-1no0)

### Extensions

* [A next-generation plugin manager for zsh](https://github.com/zplug/zplug)
* [fzf](https://github.com/junegunn/fzf) *fzf is a general-purpose command-line fuzzy finder.*

