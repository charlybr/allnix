# Automation / Automatisation

## N8N - Workflow Automation Tool

n8n est un zapier/ifttt open source écrit en nodejs.

* [Page github](https://github.com/KalvadTech/n8n.kalvad.tech)
* [Automation: n8n strikes back](https://blog.kalvad.com/automation-n8n-strikes-back/)
