
# Bash aliases I like

# show hidden files and human readable file size
alias ll="ls -lah"

# Find modified files last 24h in current directory
alias find-today="find . -mtime -1 -ls"

# current weather
alias meteo="curl 'https://wttr.in/Reims?lang=fr'"

# get father's pid of a process
# ppid <id>
alias ppid='ps -o ppid= -p'
