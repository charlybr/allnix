#  Bash dotfile

## Configuration de démarrage

Lors de l'execution interactive ou non de bash, il va aller lire un certain nombre de fichiers de configuration comme :

* `/etc/profile`
* `~/.bash_profile`
* `~/.bash_login`
* `~/.profile`
* `~/.bashrc`

`rc` pour la petite histoire signifie `run commands` et est utilisé pour exécuter des commandes/scripts au démarrage de bash.

Pour le détails de ces fichiers et l'ordre de leur utilisation, voir [la page de manuel bash](https://www.gnu.org/software/bash/manual/html_node/Bash-Startup-Files.html).

## fichiers de configuration personnalisés

* [.bash_aliases](.bash_aliases) des alias que j'aime bien utiliser

## Boilerplate

* [Minimal safe Bash script template](https://betterdev.blog/minimal-safe-bash-script-template/) *2020-12-14*
* [Bash3 boilerplate](https://github.com/kvz/bash3boilerplate)
* [Bash boilerplate](https://github.com/xwmx/bash-boilerplate)
