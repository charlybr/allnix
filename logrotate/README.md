# Logrotate

* Page man https://linux.die.net/man/8/logrotate

## Tester une configuration

```
logrotate -f /etc/logrotate.d/my-config
```

Attention, cela applique la rotation.

## Options de configuration

* gestion des permissions

```
create 0664 www-data www-data
```

* compresser les logs journalisés
```
compress
```

* garder le `fichier.log.1` non compressé
```
delaycompress
```

* rotation quotidienne 
```
daily
```

* nombre d'archives conservées
```
rotate 7
```

## Docs

* https://www.baeldung.com/linux/rotating-logs-logrotate
