# Pulumi

*Pulumi is a modern infrastructure as code platform that allows you to use familiar programming languages and tools to build, deploy, and manage cloud infrastructure.*

* [Get started with Pulumi](https://www.pulumi.com/docs/get-started/)
* Pulumi for Terraform users https://www.youtube.com/watch?v=PqAP4BunQZU
