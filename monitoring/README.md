# Monitoring

## Outils open source

* [Nagios](nagios.md) 
  * l'ancêtre des outils de monitoring. écrit en C et Perl. [homepage](https://www.nagios.org/)
  * fonctionnement serveur qui pull des agents
* [Icinga2](incinga2.md)
  * fork de Nagios, le core d'icinga2 est réécrit en C++ [homepage](https://icinga.com/get-started/)
* Netdata
* [Prometheus](prometheus/README.md)
* Smokeping [homepage](https://oss.oetiker.ch/smokeping/)
  * Installation sur debian 11 https://sleeplessbeastie.eu/2021/08/27/how-to-install-smokeping-on-debian-bullseye/
* Zabbix
  * agent-serveur. écrit en C, PHP et Java
* CheckMK [homepage](https://checkmk.com/)
* HyperDX https://github.com/hyperdxio/hyperdx *An open source observability platform unifying session replays, logs, metrics, traces and errors.*

Voir https://geekflare.com/fr/best-open-source-monitoring-software/


## Articles sur le monitoring

* [Push vs. Pull Monitoring Configs](https://steve-mushero.medium.com/push-vs-pull-configs-for-monitoring-c541eaf9e927)
* https://thenewstack.io/monitoring-vs-observability-whats-the-difference/

## Pages de statut

* https://github.com/cstate/cstate
  * à base de Hugo
* https://cachethq.io/
  * PHP/Laravel
