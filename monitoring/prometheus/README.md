# Prometheus

* Base de données timeseries, moteur "sql" et interface web.
* écrit en Go
* Pas d'agent, il va récupérer (scrapper) les datas sur des urls (routes)
  * prometheus s'attend à scrapper du contenu sur des routes du type `host:9090/metrics` 
* [Site officiel](https://prometheus.io/)
* Repo https://github.com/prometheus/prometheus
* [Prometheus Cheatsheets](http://blog.ruanbekker.com/cheatsheets/prometheus/)

## Configuration

* `/etc/prometheus/prometheus.yml`

## Documentation

* https://xavki.blog/debuter-avec-prometheus-et-se-former/
* [Prometheus/Grafana par Xavki](https://www.youtube.com/watch?v=wcTr8Hm7SCQ&list=PLn6POgpklwWo3_2pj5Jflqwla62P5OI8n&ab_channel=xavki)

## Docker

```
$ docker run \
    -p 9090:9090 \
    -v /path/to/prometheus.yml:/etc/prometheus/prometheus.yml \
    -v /path/to/data:/prometheus \
    quay.io/prometheus/prometheus:latest
```
