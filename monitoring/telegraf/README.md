# Telegraf

* [Github](https://github.com/influxdata/telegraf)
* [Official configuration doc](https://github.com/influxdata/telegraf/blob/master/docs/CONFIGURATION.md)

## Collecte de données

* [How to Time Your Data Collection with Telegraf Agent Settings](https://www.influxdata.com/blog/how-to-time-data-collection-telegraf/)
* [Different time interval for each telegraf input](https://community.influxdata.com/t/different-time-interval-for-each-telegraf-input/23313)
