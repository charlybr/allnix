# Icinga2

## Installation

* [How to Install Icinga2 Monitoring Tool on Ubuntu 20.04/22.04](https://www.tecmint.com/install-icinga-monitoring-ubuntu/)
* [Ansible playbooks](https://github.com/Icinga/ansible-playbooks)


## Commandes

* vérifier la configuration

```
icinga2 daemon -C
```
