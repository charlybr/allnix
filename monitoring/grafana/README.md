# Grafana

## Plugins

* [Table Panel](https://grafana.com/grafana/plugins/table/) 
  * [docs](https://grafana.com/docs/grafana/v7.5/panels/visualizations/table/) 
  * [demo](https://play.grafana.org/d/U_bZIMRMk/table-panel-showcase?orgId=1)
* [k8s](https://grafana.com/grafana/plugins/grafana-kubernetes-app/)
* [SVG panel](https://github.com/MarcusCalidus/marcuscalidus-svg-panel)
* [Boom table](https://grafana.com/grafana/plugins/yesoreyeram-boomtable-panel/)
* [Flow charting](https://grafana.com/grafana/plugins/agenty-flowcharting-panel/)
