# Git

## Apprendre Git

* https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud

## Articles

* [Git Organized: A Better Git Flow](https://dev.to/render/git-organized-a-better-git-flow-56go)
* [What's the difference between git clone --mirror and git clone --bare](https://stackoverflow.com/questions/3959924/whats-the-difference-between-git-clone-mirror-and-git-clone-bare)
* [What is a bare git repository?](https://www.saintsjd.com/2011/01/what-is-a-bare-git-repository/)
* [Here are 19 that any developer should know.](https://twitter.com/oliverjumpertz/status/1563164729589534721?s=27&t=lZ0r7b2Yn-v1SasfdvP0ZA)

## Outils

* [Delta](https://github.com/dandavison/delta) *A syntax-highlighting pager for git, diff, and grep output*
* [Lazygit](https://github.com/jesseduffield/lazygit) *simple terminal UI for git commands*
* [Gitfluence](https://gitfluence.com/) décrire ce que l'on veut faire et l'outil trouve la bonne commande git associée

## Hosting

* https://gitea.io/
  * *Gitea is a community managed lightweight code hosting solution written in Go. It is published under the MIT license.*

# Snippets

## diff

* différence entre 2 commits

```
$ git diff 75beca1d e5641f84
```

voir seulement les stats

```
$ git diff --stat 75beca1d e5641f84
```

voir pour un chemin donné 

```
$ git diff 75beca1d e5641f84 payment/
```

## branches locales/remote

* trouver les branches locales qui n'existent plus sur l'`origin`

```
$ git branch -vv
```
Cette commande affiche les branches locales avec les informations liées à la branche remote.

Par exemple :
```
$ git branch -vv
  2814-cvi        0e5ecd488 [origin/2814-cvi: gone] feat: sync charts versions
* master          7d87d093d [origin/master] [BOT] Update for demo.
```

On voit que la branche `2814-cvi` a été supprimée sur l'origine, via l'information `: gone`.

## Supprimer les branches locales qui n'existent plus en remote


```
git config --global fetch.prune true
```

## Supprimer un fichier de l'historique

Cas d'usage, vous avez fait plusieurs commit en local dont un qui contenait un fichier volumineux. Vous ne pouvez plus synchroniser avec github par exemple car le fichier est trop gros. Vous pouvez utiliser la commande suivante pour supprimer le fichier de l'historique.

```
$ git filter-branch --index-filter "git rm -rf --cached --ignore-unmatch path/to/file" HEAD
```

Exemple :

```
$ git filter-branch --index-filter 'git rm -rf --cached --ignore-unmatch indesign/cookbook\ template.indd' HEAD
WARNING: git-filter-branch has a glut of gotchas generating mangled history
	 rewrites.  Hit Ctrl-C before proceeding to abort, then use an
	 alternative filtering tool such as 'git filter-repo'
	 (https://github.com/newren/git-filter-repo/) instead.  See the
	 filter-branch manual page for more details; to squelch this warning,
	 set FILTER_BRANCH_SQUELCH_WARNING=1.
Proceeding with filter-branch...

Rewrite a80ae27c604717d3950e04a90768541f47f5f6d7 (174/175) (12 seconds passed, remaining 0 predicted)    rm 'indesign/cookbook template.indd'
Rewrite ec4fa6f78b56c6173f5a5624dff5453e232d4b8a (174/175) (12 seconds passed, remaining 0 predicted)
Ref 'refs/heads/master' was rewritten
```
