# Gestion des disques

* [Comprendre la technologie SSD : NVMe, SATA, M.2](https://www.kingston.com/fr/community/articledetail/articleid/48543)

## Lister / trouver les modèles

* avec lshw [man](https://linux.die.net/man/1/lshw)

```
$ lshw -C disk
  *-disk                    
       description: ATA Disk
       product: SK hynix SC300 M
       physical id: 0.0.0
       bus info: scsi@2:0.0.0
       logical name: /dev/sda
       version: 0P00
       serial: FI62N021112802F1U
       size: 238GiB (256GB)
       capabilities: gpt-1.00 partitioned partitioned:gpt
       configuration: ansiversion=5 guid=20dc0b9a-0134-4c84-8988-b79c46614305 logicalsectorsize=512 sectorsize=512

```

version courte

```
$ lshw -short -C disk
H/W path           Device      Class          Description
=========================================================
/0/100/17/0.0.0    /dev/sda    disk           256GB SK hynix SC300 M
```

* avec udiskctl

```
$ udisksctl status
MODEL                     REVISION  SERIAL               DEVICE
--------------------------------------------------------------------------
Samsung SSD 850           2B6Q      S2RBNX0K125517R      sda
Samsung SSD 850           2B6Q      S2RBNX0K125505M      sdb
```

* avec blkid [man](https://linux.die.net/man/8/blkid)

```
$ blkid
/dev/sda1: UUID="DC00-6C1D" TYPE="vfat" PARTLABEL="EFI system partition" PARTUUID="dd5536c6-3bd0-42cd-881b-b8c0d4415f3d"
/dev/sda3: TYPE="BitLocker" PARTLABEL="Basic data partition" PARTUUID="09712364-ba44-431c-aa7c-0398f7a8413f"
/dev/sda5: UUID="b2090a76-c7cf-4141-bd6c-6dd640b1b5a9" TYPE="ext4" PARTUUID="31720e7e-6c00-4290-8661-3688ebb9a975"
/dev/sda6: UUID="9df6e991-a118-46fe-8b9d-063c7bec24e6" TYPE="swap" PARTUUID="1e10d750-0758-4a3b-8411-9639c9cdc8ad"
/dev/sda2: PARTLABEL="Microsoft reserved partition" PARTUUID="e9ed79e2-b129-4cc0-bff9-7ff4af6d0381"
/dev/sda4: PARTUUID="5d3272d2-2260-4bd0-9da8-44f8873abf40"
```

* avec hdparm

```
$ hdparm -i /dev/sda4

/dev/sda4:

 Model=SK hynix SC300 M.2 2280 256GB, FwRev=20100P00, SerialNo=FI62N021112802F1U
 Config={ Fixed }
 RawCHS=16383/16/63, TrkSize=0, SectSize=0, ECCbytes=0
 BuffType=unknown, BuffSize=unknown, MaxMultSect=16, MultSect=off
 (maybe): CurCHS=16383/16/63, CurSects=16514064, LBA=yes, LBAsects=500118192
 IORDY=on/off, tPIO={min:120,w/IORDY:120}, tDMA={min:120,rec:120}
 PIO modes:  pio0 pio3 pio4 
 DMA modes:  mdma0 mdma1 mdma2 
 UDMA modes: udma0 udma1 udma2 udma3 udma4 udma5 *udma6 
 AdvancedPM=yes: disabled (255) WriteCache=enabled
 Drive conforms to: Unspecified:  ATA/ATAPI-2,3,4,5,6,7

 * signifies the current active mode
```

* avec smartctl [man](https://linux.die.net/man/8/smartctl)

```
$ smartctl -i /dev/sda
smartctl 6.6 2017-11-05 r4594 [x86_64-linux-4.19.0-17-amd64] (local build)
Copyright (C) 2002-17, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     SK hynix SATA SSDs
Device Model:     SK hynix SC300 M.2 2280 256GB
Serial Number:    FI62N021112802F1U
Firmware Version: 20100P00
User Capacity:    256,060,514,304 bytes [256 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    Solid State Device
Form Factor:      M.2
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ATA8-ACS (minor revision not indicated)
SATA Version is:  SATA 3.1, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Thu Aug 12 09:50:54 2021 CEST
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
```

## Fragmentation

* [Utilities for Handling Disk Fragmentation in Linux](https://www.wearediagram.com/blog/utilities-for-handling-disk-fragmentation-in-linux)

## Espace réservé

Les systèmes de fichiers type `ext[34]` ont un espace réservé pour que certains processus important continuent de s'exécuter quand l'espace disque vient à manquer. Cet espace s'exprime en nombre de blocks. Par défaut, il est configuré à 5% de la taille de la partition.

Pour connaitre le nombre de blocks réservés sur une partition :
```
$ tune2fs -l /dev/sda2 | grep Reserved
Reserved block count:     65497
Reserved GDT blocks:      639
Reserved blocks uid:      0 (user root)
Reserved blocks gid:      0 (group root)
```
Ici, 65497 blocks.
Pour récupérer la taille d'un block :
```
$ blockdev --getbsz /dev/sda2
4096
```
Si on fait le calcul, `4096 * 65497 = 268275712` soit 255Mo d'espace réservé.
La partition fait :
```
$ df -h /dev/sda2
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda2       4.9G  2.9G  1.8G  62% /
```
255Mo sur 5Go, cela représente bien environ 5%.

Pour mettre à jour cette valeur, par exemple, passer à 4% :
```
$ tune2fs -m4 /dev/sda2
tune2fs 1.46.5 (30-Dec-2021)
Setting reserved blocks percentage to 4% (52398 blocks)
```

## Faire des tests I/O de disques

```
$ dd if=/dev/zero of=sb-io-test bs=64k count=16k conv=fdatasync; rm -rf sb-io-test
16384+0 records in
16384+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 3.02001 s, 356 MB/s
```

