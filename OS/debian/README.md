# Debian

## APT

### UnattendedUpgrades

* https://wiki.debian.org/UnattendedUpgrades

### Lister les paquets récemment intallés

* le log de ce qu'il se passe avec apt/dpkg est dans `/var/log/dpkg.log`

```
$ grep "\ installed\ " dpkg.log
...
2023-06-06 13:41:53 status installed libutempter0:amd64 1.1.6-4
2023-06-06 13:41:54 status installed sysstat:amd64 12.2.0-2ubuntu0.2
2023-06-06 13:41:54 status installed tmux:amd64 3.0a-2ubuntu0.4
2023-06-06 13:41:54 status installed iftop:amd64 1.0~pre4-6build1
2023-06-06 13:41:54 status installed systemd:amd64 245.4-4ubuntu3.21
2023-06-06 13:41:54 status installed mime-support:all 3.64ubuntu1
```

### Archives

* [Debian archive](https://www.debian.org/distrib/archive.html)
* [Mirror size](https://www.debian.org/mirror/size)

## Créer des packages

A partir de binaires :
* https://www.internalpointers.com/post/build-binary-deb-package-practical-guide

## Créer un repository perso

* [DebianRepositorySetup](https://wiki.debian.org/DebianRepository/Setup)
* [Creating and hosting your own deb packages and apt repo](https://earthly.dev/blog/creating-and-hosting-your-own-deb-packages-and-apt-repo/)
* [Simple DebianRepository - Déployer un dépôt Debian en 2min](https://thebidouilleur.xyz/blog/DebianRepository/)
