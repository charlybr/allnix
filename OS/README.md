# OS

## Linux

* [Exploring the internals of Linux v0.01](https://seiya.me/blog/reading-linux-v0.01)
* [Debian](debian/README.md)
* [A disk is a bunch of bits](https://www.cyberdemon.org/2023/07/19/bunch-of-bits.html)

## *BSD

### Articles

* [Why we're migrating (many of) our servers from Linux to FreeBSD](https://it-notes.dragas.net/2022/01/24/why-were-migrating-many-of-our-servers-from-linux-to-freebsd/)
* [Technical reasons to choose FreeBSD over GNU/Linux](https://unixsheikh.com/articles/technical-reasons-to-choose-freebsd-over-linux.html) *2020*

## Fonctionnement

* How to implement paging https://www.youtube.com/watch?v=LKYKp_ZzlvM&ab_channel=CaseyCole
