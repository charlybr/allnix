# Taille des OS installés


## Debian

Installation de base avec ssh et system utilities.

### Debian 11

```
charlybr@debian11:~$ uname -a
Linux debian11 5.10.0-14-amd64 #1 SMP Debian 5.10.113-1 (2022-04-29) x86_64 GNU/Linux

charlybr@debian11:~$ df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda1        15G  1.4G   13G  10% /
```
