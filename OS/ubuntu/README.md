# Ubuntu

## Mettre à jour sa distro

* Mettre à jour les index des packages

```
$ sudo apt-get update
```

avant de mettre à jour, on peut lister les packages ayant une mise à jour disponible :
```
$ sudo apt list --upgradable
```

* Installer les nouvelles versions des packages

```
$ sudo apt-get upgrade
```

Voir aussi :
* [How to Setup and Enable Automatic Security Updates on Ubuntu](https://phoenixnap.com/kb/automatic-security-updates-ubuntu)
