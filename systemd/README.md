# Systemd

## Timers

Systemd implémente un système de timers pour remplacer cron.
Il se configure via des fichiers avec extension `.timer`.
On peut trouver les timers dans `/etc/systemd/system/timers.target.wants`


* Voir les timers actifs
```
$ systemctl list-timers
NEXT                        LEFT          LAST                        PASSED       UNIT                         ACTIVATES
Thu 2022-09-15 09:39:00 UTC 13min left    Thu 2022-09-15 09:09:00 UTC 16min ago    phpsessionclean.timer        phpsessionclean.service
Thu 2022-09-15 10:57:56 UTC 1h 31min left Thu 2022-09-15 04:33:05 UTC 4h 52min ago ua-timer.timer               ua-timer.service
Thu 2022-09-15 16:00:24 UTC 6h left       Thu 2022-09-15 05:09:36 UTC 4h 16min ago motd-news.timer              motd-news.service
Fri 2022-09-16 00:00:00 UTC 14h left      Thu 2022-09-15 00:00:01 UTC 9h ago       logrotate.timer              logrotate.service
Fri 2022-09-16 05:11:37 UTC 19h left      Thu 2022-09-15 03:31:55 UTC 5h 54min ago apt-daily.timer              apt-daily.service
Fri 2022-09-16 06:43:08 UTC 21h left      Thu 2022-09-15 06:57:26 UTC 2h 28min ago apt-daily-upgrade.timer      apt-daily-upgrade.service
Fri 2022-09-16 07:58:46 UTC 22h left      Thu 2022-09-15 07:58:46 UTC 1h 27min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service
Sun 2022-09-18 03:10:06 UTC 2 days left   Sun 2022-09-11 03:10:22 UTC 4 days ago   e2scrub_all.timer            e2scrub_all.service
```

* Voir le statut d'un timer 
```
$ systemctl status logrotate.timer
● logrotate.timer - Daily rotation of log files
     Loaded: loaded (/lib/systemd/system/logrotate.timer; enabled; vendor preset: enabled)
     Active: active (waiting) since Thu 2022-08-18 07:40:33 UTC; 4 weeks 0 days ago
    Trigger: Fri 2022-09-16 00:00:00 UTC; 14h left
   Triggers: ● logrotate.service
       Docs: man:logrotate(8)
             man:logrotate.conf(5)

Warning: journal has been rotated since unit was started, output may be incomplete.
```
