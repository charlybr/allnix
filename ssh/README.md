# SSH

## Articles

* [Comparing ssh keys](https://goteleport.com/blog/comparing-ssh-keys/) *2022* TL;DR `ssh-keygen -t ed25519`
* [Mettre à jour ses clés SSH](https://medium.com/risan/upgrade-your-ssh-key-to-ed25519-c6e8d60d3c54) *2018*
* PEM est un fichier codé en Base64 utilisant des caractères ASCII.
* [An Excruciatingly Detailed Guide To SSH (But Only The Things I Actually Find Useful)](https://grahamhelton.com/blog/ssh-cheatsheet/)

## config client `~/.ssh/config`

Gérer le timeout vers un serveur :
```
Host *
    ServerAliveInterval 60
```

Indique au client d'envoyer un paquet "vide" toutes les 60 secondes au serveur pour éviter un timeout. Utile par exemple lors de l'exécution d'une commande un peu longue.

## Config serveur `/etc/ssh/sshd_config`

```
ClientAliveInterval 60
ClientAliveCountMax 240
```

Indique au serveur d'envoyer un paquet "vide" toutes les 60 secondes au client et ne déconnecte pas le client tant qu'il n'a pas été actif durant une période de 240 intervales, soit 240 * 60 = 14400 secondes, soit 4h.



## Snippets

### Créer un tunnel 

```
ssh -L 3000:127.0.0.1:3001 user@hostname
```

Cette commande établit un tunnel entre votre machine sur le port 3000 et la machine distante **hostname** sur le port **3001** de l'ip **127.0.0.1** de la machine distante.

Vous pouvez ainsi ouvrir votre navigateur sur l'adresse **localhost:3000**.

### Créer une clé au format PEM

```
ssh-keygen  -t ed25519 -m pem
```

### Convertir une clé au format PEM

```
$ ssh-keygen -p -f id_ed25519 -m pem
```


### what is the meaning of MaxStartups 10:30:60?

* 10: Number of unauthenticated connections before we start dropping
* 30: Percentage chance of dropping once we reach 10 (increases linearly for more than 10)
* 60: Maximum number of connections at which we start dropping everything

### Copier des données entre 2 hosts distants

Avec scp il est possible de copier des fichiers d'un host distant `host1` vers un second host distant `host2` en passant pas la machine `localhost` sur laquelle on initie la commande :

```
user@localhost$ scp -3 user1@remote1:~/foo.bar user2@remote2:~/
```

## Durcissement de la sécurité ssh 

* [How To Harden OpenSSH on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-harden-openssh-on-ubuntu-20-04) *2021*
* [How to Secure and Harden OpenSSH Server](https://www.tecmint.com/secure-openssh-server/) *2020*
* [Top 20 OpenSSH Server Best Security Practices](https://www.cyberciti.biz/tips/linux-unix-bsd-openssh-server-best-practices.html) *2022*

### OTP 

* [How to configure SSH with YubiKey Security Keys U2F Authentication on Ubuntu](https://cryptsus.com/blog/how-to-configure-openssh-with-yubikey-security-keys-u2f-otp-authentication-ed25519-sk-ecdsa-sk-on-ubuntu-18.04.html)
* [Are there security advantages to adding an OTP to SSH connections?](https://security.stackexchange.com/questions/209985/are-there-security-advantages-to-adding-an-otp-to-ssh-connections)
* [The problem with google authenticator](https://netknights.it/en/the-problem-with-the-google-authenticator/)
