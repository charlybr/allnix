# MariaDB / Mysql

* Apprendre le SQL https://sql.sh/
* http://mysql.rjweb.org/doc.php/index_cookbook_mysql


## Articles

### Cache


## Outils d'analyze

* https://github.com/BMDan/tuning-primer.sh
* https://github.com/major/MySQLTuner-perl

## Optimisation

* [https://dba.stackexchange.com/questions/27328/how-large-should-be-mysql-innodb-buffer-pool-size](How large should be mysql innodb_buffer_pool_size?)
* [10 reasons why MySQL Query Cache should be switched off](https://mysqlquicksand.wordpress.com/2020/05/08/10-reasons-why-mysql-query-cache-should-be-switched-off/)
* [How to Fully Disable Query Cache in MySQL](https://www.percona.com/blog/is-your-query-cache-really-disabled/)
* [MySQL: Building the best INDEX for a given SELECT ](https://mysql.rjweb.org/doc.php/index_cookbook_mysql)

## Backups

* MyDumper https://github.com/mydumper/mydumper

## Snippets

### Query Cache

* vérifier l'activation du query cache
```
mysql> SHOW VARIABLES LIKE 'have_query_cache';
+------------------+-------+
| Variable_name    | Value |
+------------------+-------+
| have_query_cache | YES   |
+------------------+-------+
```
* toutes les variables liées au query cache
```
mysql> show variables like 'query_cache_%' ;
+------------------------------+----------+
| Variable_name                | Value    |
+------------------------------+----------+
| query_cache_limit            | 1048576  |
| query_cache_min_res_unit     | 4096     |
| query_cache_size             | 16777216 |
| query_cache_type             | OFF      |
| query_cache_wlock_invalidate | OFF      |
+------------------------------+----------+
```

* `query_cache_limit` – taille maximale du cache
* `query_cache_min_res_result` – MySQL stores query result in blocks. This is the minimum size of each block.
* `query_cache_size` – indicates the total amount of memory allocated for MySQL cache.
* `query_cache_type` – setting this to 0 or OFF disables MySQL query cache. setting it to 1 enables query cache.
* `query_cache_wlock_invalidate` – determines if MySQL should fetch results from cache if the underlying table is locked.

### Calculer la taille des tables

```
SELECT 
     table_schema as `Database`, 
     table_name AS `Table`, 
     round(((data_length + index_length) / 1024 / 1024), 2) `Size in MB` 
FROM information_schema.TABLES 
ORDER BY (data_length + index_length) DESC;
```

### Limiter le temps d'exécution d'une requête

Permet de stopper une requête si elle prend plus de temps que configuré.

* https://mariadb.com/kb/en/aborting-statements/

```
mysql> SHOW variables LIKE 'max_statement_time';
```


