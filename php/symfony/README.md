# Symfony

* [Petite liste "top" des bundles que l’on retrouve souvent dans les projets Symfony](https://developpeur-freelance.io/blog/top-bundle-symfony)

## Assets

* [Supprimer Webpack, … de vos projets Symfony ?](https://codedesign.fr/tutorial/supprimer-webpack-projets-php-symfony/)

## 

* [Achieving High Performance Queue's with Symfony Messenger](https://joppe.dev/2023/01/21/symfony-messenger-high-performance-queues/)

