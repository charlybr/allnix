# Ansible

* https://www.ansible.com/ by RedHat
* [Installation guide](https://docs.ansible.com/ansible/latest/installation_guide/index.html)
  * [Getting started](https://docs.ansible.com/ansible/latest/user_guide/index.html#getting-started)
  * [Best practices](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html) *doc officielle*
  * [Best practives part 1](https://medium.com/polarsquad/ansible-best-practices-part-1-b3391b3c6f68) *polar squad*
  * [Best practices part 2](https://polarsquad.com/blog/ansible-best-practices-part-2) *polar squad*
  * [Ansible - Les meilleures pratiques - Partie 1](https://blog.stephane-robert.info/post/ansible-best-practices-tips/) *Stéphane Robert*

## Tutos / Articles

* [Ansible par Xavki](https://www.youtube.com/playlist?list=PLn6POgpklwWoCpLKOSw3mXCqbRocnhrh-)
* [Ansible Vault pour protéger vos secrets](https://blog.stephane-robert.info/post/ansible-vault/)
* [Introduction au cours complet sur Ansible](https://devopssec.fr/article/introduction-cours-complet-ansible)
* [8 steps to developing an Ansible role in Linux](https://www.redhat.com/sysadmin/developing-ansible-role) *exemple avec vim*
* [Utiliser les custom facts](https://blog.stephane-robert.info/post/ansible-custom-facts/)
* [Ansible for DevOps](https://github.com/geerlingguy/ansible-for-devops)
* [Ansible, une meilleure expérience est possible.](https://jseguillon.io/blogs/01-ansible-facile/)

## Getting started

* Machine qui déploie souvent appelée *Control node* ou *controler node*, par opposition, les noeuds sur lesquels on déploie sont appelés les *managed nodes*

### Principe d'exécution d'une commande avec ansible

* création d'un fichier python
* création d'un répertoire (?)
* envoi du fichier python via sftp
* exécution du fichier python sur le node
* récupération du résultat

### Configuration

* fichiers d'exemple https://github.com/ansible/ansible/tree/devel/examples
  * https://github.com/ansible/ansible/blob/devel/examples/ansible.cfg
* [Gérer et Maitriser les inventaires Ansible](https://blog.stephane-robert.info/post/ansible-inventaire-static-precedence-tips/)
* https://stackoverflow.com/questions/32830428/where-should-i-be-organizing-host-specific-files-templates

Changes can be made and used in a configuration file which will be searched for in the following order:

* `ANSIBLE_CONFIG` (environment variable if set)
* `ansible.cfg` (in the current directory)
* `~/.ansible.cfg` (in the home directory)
* `/etc/ansible/ansible.cfg`

### Glossaire

* Inventory : inventaire des machines
* playbook : ensemble de scripts/fichiers de configuration pour gérer la configuration de serveurs.
```
An Ansible playbook is an organized unit of scripts that defines work for a server configuration managed by the automation tool Ansible. Ansible is a configuration management tool that automates the configuration of multiple servers by the use of Ansible playbooks.
```
* Tasks : faire des actions
* Modules : actions ciblées pour un outil
* Roles : ensemble d'actions

### ansible.cfg

éléments de base pour la configuration

* `inventory` : liste des managed hosts
* `forks` : nombre de forks max pour exécuter les commandes sur les managed nodes
* `sudo_user` : 
* `ask_pass` : 
* `host_key_checking` : pour activer, désactiver la vérification des clés de hosts en ssh
* `callback_whitelist = profile_tasks` pour profiler les appels aux commandes
* ssh
```
[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o PreferredAuthentications=publickey
```

### Variables d'environnement

* `ANSIBLE_VAULT_PASSWORD_FILE` is storing the path the the vault password file. This way you won't have the always use the `--vault-password-file` switch when running a playbook.
* `ANSIBLE_PRIVATE_KEY_FILE` la clé ssh utilisée pour se connecter aux machines

### ansible-config

* voir la configuration actuelle

```
$ ansible-config view
```

* dumper la config complète avec les variables par défaut

```
$ ansible-config dump
```

* voir seulement les valeurs modifiées par rapport aux valeurs par défaut

```
$ ansible-config dump --only-changed
```

## commandes

Ping des serveurs :
```
ansible all -m ping
```

### Exécuter un rôle

```
ansible -i hosts -m include_role -a name=telegraf-pkg ipfs-dev
```

### Exécuter un playbook sur un groupe

```
ansible-playbook -i hosts playbooks/ipfs.yml -l ipfs-alphanet
```

### Lister les hosts sur lesquels ansible va s'exécuter
```
ansible-playbook -i hosts playbooks/ipfs.yml -l ipfs-alphanet --list-hosts
playbook: playbooks/ipfs.yml

  play #1 (all): Deploy IPFS infra	TAGS: []
    pattern: ['all']
    hosts (2):
      test-ipfs-charles-1
      test-ipfs-charles-0
```

## Editer des fichiers

Il existe plusieurs modules pour modifier des fichiers :
* [lineinfile](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html) ajouter/modifier/supprimer une ligne
* [replace](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/replace_module.html) ajouter/modifier/supprimer plusieurs lignes
* [blockinfile](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/blockinfile_module.html) ajouter des blocks de lignes

## Exemples

### déployer une clé ssh 

```
- name: Set authorized key
  ansible.posix.authorized_key:
    user: foo
    state: present
    key: "ssh-ed25519 AAAAA.....0 foo@bar"
```

on peut faire une boucle pour autoriser plusieurs clés : 
```

- name: Set authorized key
  ansible.posix.authorized_key:
    user: foo
    state: present
    key: "{{ item }}"
  loop:
    - "ssh-ed25519 AAAAA.....1 foo1@localhost"
    - "ssh-rsa AAAAA.....2 foo2@localhost"
    - "ssh-dsa AAAAA.....3 foo3@localhost"
```

## A savoir

* [Permanently Set Remote System Wide Environment Variables on Linux - /etc/environment - Ansible module lineinfile](https://www.ansiblepilot.com/articles/permanently-set-remote-system-wide-environment-variables-on-linux-ansible-module-lineinfile/)


## Copie de fichiers

* [Ansible Copy Examples – How to copy files and directories with Ansible](https://www.middlewareinventory.com/blog/ansible-copy-examples/)
