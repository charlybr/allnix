# IPFS

Les ports utilisés par IPFS :
* 4001 – Communication avec les autres nodes
* 5001 – API server
* 8080 – Gateway server

Les ports utilisés par IPFS cluster :
* 9094 – HTTP API endpoint
* 9095 – IPFS proxy endpoint
* 9096 – Cluster swarm, used for communication between cluster nodes

## IPFS Cluster

* [IPFS Tutorial: Building a Private IPFS Network with IPFS-Cluster for Data Replication](https://labs.eleks.com/2019/03/ipfs-network-data-replication.html)
