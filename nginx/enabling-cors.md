# Activer CORS 

* [Cross-origin resource sharing (CORS)](https://developer.mozilla.org/fr/docs/Web/HTTP/CORS)

Pour autoriser tous les domaines :

```
server {
   [...]

   add_header Access-Control-Allow-Origin *;

   [...]
}
```

Pour autoriser des domaines spécifiques :
```
server {
   [...]
    add_header Access-Control-Allow-Origin "example1.com";
    add_header Access-Control-Allow-Origin "example2.com";
    add_header Access-Control-Allow-Origin "example3.com";
    [...]
}
```
