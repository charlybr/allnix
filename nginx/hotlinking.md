# Empêcher le hotlinking

```
location ~ .(gif|png|jpe?g)$ {
   valid_referers none blocked domain.com *.domain.com;
   if ($invalid_referer) {
      return 403;
   }
}
```

