# Nginx

## Configuration

* [Configuring Nginx for Performance and Security](https://geekflare.com/nginx-production-configuration/)

## Optimisation 

* [Nginx tuning](https://github.com/denji/nginx-tuning)
* [Optimizing Nginx for High Traffic Loads](https://blog.martinfjordvald.com/optimizing-nginx-for-high-traffic-loads/)

## Durcissement

* https://geekflare.com/nginx-webserver-security-hardening-guide/
* https://beaglesecurity.com/blog/article/nginx-server-security.html
* [Top 25 Nginx Web Server Best Security Practices](https://www.cyberciti.biz/tips/linux-unix-bsd-nginx-webserver-security.html)

## Cache

Ajouter un header HTTP qui indique le statut du cache pour la requete
```
add_header X-Cache-Status $upstream_cache_status;
```

Valeurs possibles : MISS, BYPASS, EXPIRED, STALE, UPDATING, REVALIDATED, HIT. ([documentation officielle](http://nginx.org/en/docs/http/ngx_http_upstream_module.html))

## Upload

```
server {
    ...
    client_max_body_size 100M;
}
```

### Articles

* https://serverfault.com/questions/583570/understanding-the-nginx-proxy-cache-path-directive

## Exemples de configuration

* [Redirection du traffic http vers https](nginx/80-to-443.md)

* retourner un code HTTP spécifique

```
# return 403
location ~ ^/ {
    deny all;
}

location ^~ /forum { return 410; }
```


