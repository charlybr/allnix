# Sécurité des systèmes

## Scanners de vulnérabilités

* [OpenVAS](https://www.openvas.org/)
* [Nessus](https://fr.tenable.com/products/nessus)
* [Nikto](https://cirt.net/Nikto2)
* [Burp Suite](https://portswigger.net/burp/communitydownload)


## Gestion des mots de passe

* [Le changement de mot passe, c'est maintenant avec Vaultwarden](https://bearstech.com/societe/blog/le-changement-de-mot-passe-cest-maintenant-avec-vaultwarden/)
