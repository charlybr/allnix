# Python

## Apprendre Python

* [Learn Python - Full Course for Beginners](https://www.youtube.com/watch?v=rfscVS0vtbw&ab_channel=freeCodeCamp.org)
* [Learn Python’s more advanced features by tinkering with some demo snippets, for a few minutes each day](https://github.com/Aviah/python-dig)
* [Clean Code concepts adapted for Python](https://github.com/zedr/clean-code-python)

## Comment faire

* [How to Execute Shell Commands with Python](https://janakiev.com/blog/python-shell-commands/) *2019*

## Tutos

* [Python DevOps tutorials](https://realpython.com/tutorials/devops/)

## Logs

* [Logging in Python like a PRO](https://guicommits.com/how-to-log-in-python-like-a-pro/)
