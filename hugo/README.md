# Hugo CMS

* https://gohugo.io/

## Thèmes

* https://retrolog.io/blog/creating-a-hugo-theme-from-scratch/

## Snippets

* Tester le site en local

```
$ hugo -D server
```

* Générer les fichiers statiques
```
$ hugo
```

