# Kubernetes

* [Formation Kubernetes : orchestrer des conteneurs par Xavki](https://www.youtube.com/playlist?list=PLn6POgpklwWqfzaosSgX2XEKpse5VY2v5)
* [Introduction Kubernetes](https://blog.stephane-robert.info/post/introduction-kubernetes/) par Stéphane Robert
* [Cluster Kubernetes : architecture et composants de base](https://www.nicolashug.com/kubernetes/cluster-kubernetes-architecture-et-composants-de-base/)
* [Introducing Bare Metal Kubernetes: what you need to know](https://www.spectrocloud.com/blog/introducing-bare-metal-kubernetes-what-you-need-to-know) *2022-08-17*
* [Kubernetes Tutorial for Beginners [FULL COURSE in 4 Hours]](https://www.youtube.com/watch?v=X48VuDVv0do&ab_channel=TechWorldwithNana)
* [Kubernetes on bare-metal in 10 minutes](https://blog.alexellis.io/kubernetes-in-10-minutes/) *2017-06-28*
* [Top 10 Must-Have Tools for Kubernetes Engineers](https://semaphoreci.medium.com/top-10-must-have-tools-for-kubernetes-engineers-71b32fd21b95)

## Gestionnaires d'applications

* [Kapp - Le gestionnaire d’applications Kubernetes](https://blog.stephane-robert.info/post/kubernetes-kapp-manage-application/)
* [ArgoCD](https://github.com/argoproj/argo-cd)
# Kubernetes

## Apprendre Kubernetes "k8s"

### Docs / articles

* https://kubernetes.io/docs/concepts/workloads/pods/init-containers/ *This page provides an overview of init containers: specialized containers that run before app containers in a Pod. Init containers can contain utilities or setup scripts not present in an app image.*
* [Dessine-moi un cluster](https://github.com/jpetazzo/dessine-moi-un-cluster#dessine-moi-un-cluster)
* [Demystifions Kubernetes](https://github.com/zwindler/demystifions-kubernetes)
* [Plain Kubernetes Secrets are fine](https://www.macchaffee.com/blog/2022/k8s-secrets/)
* [LISA19 - Deep Dive into Kubernetes Internals for Builders and Operators](https://www.youtube.com/watch?v=3KtEAa7_duA&ab_channel=USENIX)

### Kubernetes The Hard Way 

This tutorial walks you through setting up Kubernetes the hard way. This guide is not for people looking for a fully automated command to bring up a Kubernetes cluster. If that's you then check out Google Kubernetes Engine, or the Getting Started Guides.

* :uk: https://github.com/kelseyhightower/kubernetes-the-hard-way

## CD / Continuous delivry

* https://argo-cd.readthedocs.io/en/stable/

## Outils

* [Minikube vs. kind vs. k3s - What should I use?](https://shipit.dev/posts/minikube-vs-kind-vs-k3s.html) *2019*

### Minikube

* https://minikube.sigs.k8s.io/docs/ minikube quickly sets up a local Kubernetes cluster on macOS, Linux, and Windows.
* [How to use minikube for basic kubernetes](https://www.airplane.dev/blog/using-minikube-for-basic-kubernetes) *2021*

## Package Manager

### Helm

* https://helm.sh/
* [13 Best Practices for using Helm](https://codersociety.com/blog/articles/helm-best-practices)

## Cheatsheet

### contextes

* lister les contextes

```
kubectl config get-contexts
```

juste le nom du contexte 
```
kubectl config get-contexts -o=name
```

* changer de contexte

```
kubectl config set-context [context]
```

* récupérer le contexte courant

```
kubectl config current-context
```

